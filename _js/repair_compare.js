jQuery(document).ready(function($){
    //check if the .repair_photos_container is in the viewport 
    //if yes, animate it
    checkPosition(jQuery('.repair_photos_container'));
    jQuery(window).on('scroll', function(){
        checkPosition(jQuery('.repair_photos_container'));
    });
    
    //make the .repair-handle element draggable and modify .repair-resize-img width according to its position
    jQuery('.repair_photos_container').each(function(){
        var actual = jQuery(this);
        drags(actual.find('.repair-handle'), actual.find('.repair-resize-img'), actual, actual.find('.repair_photos_label[data-type="original"]'), actual.find('.repair_photos_label[data-type="modified"]'));
    });

    //upadate images label visibility
    jQuery(window).on('resize', function(){
        jQuery('.repair_photos_container').each(function(){
            var actual = jQuery(this);
            updateLabel(actual.find('.repair_photos_label[data-type="modified"]'), actual.find('.repair-resize-img'), 'left');
            updateLabel(actual.find('.repair_photos_label[data-type="original"]'), actual.find('.repair-resize-img'), 'right');
        });
    });

    function checkPosition(container) {
        container.each(function(){
            var actualContainer = jQuery(this);
            if( jQuery(window).scrollTop() + jQuery(window).height()*0.5 > actualContainer.offset().top) {
                actualContainer.addClass('is-visible');
            }
        });
    }

    //draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
    function drags(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
        dragElement.on("mousedown vmousedown", function(e) {
            dragElement.addClass('draggable');
            resizeElement.addClass('resizable');

            var dragWidth = dragElement.outerWidth(),
                xPosition = dragElement.offset().left + dragWidth - e.pageX,
                containerOffset = container.offset().left,
                containerWidth = container.outerWidth(),
                minLeft = containerOffset + 10,
                maxLeft = containerOffset + containerWidth - dragWidth - 10;
            
            dragElement.parents().on("mousemove vmousemove", function(e) {
                leftValue = e.pageX + xPosition - dragWidth;
                
                //constrain the draggable element to move inside his container
                if(leftValue < minLeft ) {
                    leftValue = minLeft;
                } else if ( leftValue > maxLeft) {
                    leftValue = maxLeft;
                }

                widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';
                
                jQuery('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
                    jQuery(this).removeClass('draggable');
                    resizeElement.removeClass('resizable');
                });

                jQuery('.resizable').css('width', widthValue); 

                updateLabel(labelResizeElement, resizeElement, 'left');
                updateLabel(labelContainer, resizeElement, 'right');
                
            }).on("mouseup vmouseup", function(e){
                dragElement.removeClass('draggable');
                resizeElement.removeClass('resizable');
            });
            e.preventDefault();
        }).on("mouseup vmouseup", function(e) {
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
    }

    function updateLabel(label, resizeElement, position) {
        if(position == 'left') {
            ( label.offset().left + label.outerWidth() < resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
        } else {
            ( label.offset().left > resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
        }
    }
});