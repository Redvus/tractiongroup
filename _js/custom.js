jQuery(document).ready(function($) {

	//Button Up
	jQuery('<div id="buttonUp"></div>').insertAfter('.wrapper');
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() >= 600) {
			jQuery('#buttonUp').css({
				'-webkit-transform': 'translateX(-20px)',
				'-moz-transform':'translateX(-20px)',
				'-o-transform':'translateX(-20px)',
				'transform':'translateX(-20px)',
				'opacity':'1'
			});
			}
			else {
				jQuery('#buttonUp').css({
					'-webkit-transform': 'translateX(20px)',
					'-moz-transform':'translateX(20px)',
					'-o-transform':'translateX(20px)',
					'transform':'translateX(20px)',
					'opacity':'0'
				});
				};
			});
	jQuery('#buttonUp').click(function() {
		$('body,html').animate({scrollTop: 0}, 400);
	});
	
	jQuery('.remont-menu img').tooltip({
		placement: 'top'
	});
	
	jQuery('.repair_mail-icon img, #description_ram, #description_eq, .tg_tooltip_up').tooltip({
		placement: 'top'
	});
	
	var is_touch_device = ("ontouchstart" in window) || window.DocumentTouch && document instanceof DocumentTouch;
	$("#description_tel").popover({
	    trigger: is_touch_device ? "click" : "hover",
	    placement: 'top'
	}).css({'cursor':'pointer'});
	
	/*
jQuery('#description_tel').popover({
		placement: 'top',
		trigger:'hover'
	}).css({'cursor':'pointer'});
*/
	
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() >= 300) {
			jQuery('.remont-menu').css({
				'-webkit-transform': 'translateX(-30px)',
				'-moz-transform':'translateX(-30px)',
				'-o-transform':'translateX(-30px)',
				'transform':'translateX(-30px)',
				'opacity':'1'
			});
			}
			else {
				jQuery('.remont-menu').css({
					'-webkit-transform': 'translateX(30px)',
					'-moz-transform':'translateX(30px)',
					'-o-transform':'translateX(30px)',
					'transform':'translateX(30px)',
					'opacity':'0'
				});
				};
			});
			
	//Модальные окна контактов
	//jQuery('#myModal').modal('hide');
	
	//Схемы проезда и переключение на карту
	jQuery('#button_trak').click(function() {
		jQuery('#scheme_trak').animate({
			'opacity':'toggle'
		}, {
			duration: 200,
			specialEasing: {
		      opacity: 'linear'
		    }
		});
	});
	
	jQuery('#button_euro').click(function() {
		jQuery('#scheme_euro').animate({
			'opacity':'toggle'
		}, {
			duration: 200,
			specialEasing: {
		      opacity: 'linear'
		    }
		});
	});
	
	jQuery('#button_man').click(function() {
		jQuery('#scheme_man').animate({
			'opacity':'toggle'
		}, {
			duration: 200,
			specialEasing: {
		      opacity: 'linear'
		    }
		});
	});
	
	jQuery('#button_hino').click(function() {
		jQuery('#scheme_hino').animate({
			'opacity':'toggle'
		}, {
			duration: 200,
			specialEasing: {
		      opacity: 'linear'
		    }
		});
	});
	
	jQuery('#button_motor').click(function() {
		jQuery('#scheme_motor').animate({
			'opacity':'toggle'
		}, {
			duration: 200,
			specialEasing: {
		      opacity: 'linear'
		    }
		});
	});
	
	jQuery('#button_bosch').click(function() {
		jQuery('#scheme_bosch').animate({
			'opacity':'toggle'
		}, {
			duration: 200,
			specialEasing: {
		      opacity: 'linear'
		    }
		});
	});
	
	jQuery('#button_trak, #button_euro, #button_man, #button_hino, #button_motor, #button_bosch').tooltip({
		placement: 'top'
	});
	
	//scroll to 
	jQuery('#description_ram').click(function(){
		var el = jQuery('#ram');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#description_eq').click(function(){
		var el = jQuery('#equipment');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#description_eq2').click(function(){
		var el = jQuery('#equipment');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#evacuation_link').click(function(){
		var el = jQuery('#repair_phones');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,160);
		return false;
	});
	
	jQuery('#buttonEvacuation').click(function(){
		var el = jQuery('#evacuation');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#buttonDefect').click(function(){
		var el = jQuery('#defect');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#buttonRam').click(function(){
		var el = jQuery('#ram');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#buttonCabin').click(function(){
		var el = jQuery('#cabin');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#buttonPaint').click(function(){
		var el = jQuery('#paint');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#buttonAxis').click(function(){
		var el = jQuery('#axis');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#buttonEnsurance').click(function(){
		var el = jQuery('#ensurance');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#buttonEquipment').click(function(){
		var el = jQuery('#equipment');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,80);
		return false;
	});
	
	jQuery('#buttonMail').click(function(){
		var el = jQuery('#repair-mail');
		var elWrapped = jQuery(el);
		scrollToDiv(elWrapped,180);
		return false;
	});
	
	function scrollToDiv(element,navheight){
		var offset = element.offset();
		var offsetTop = offset.top;
		var totalScroll = offsetTop-navheight;
		
		jQuery('body,html').animate({
			scrollTop: totalScroll
		}, 500);
	}
	
	
	
});