;(function () {

    //Button Up
	jQuery('<div id="buttonUp"></div>').insertAfter('.wrapper');
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() >= 600) {
			jQuery('#buttonUp').css({
				'opacity':'1'
			});
			}
			else {
				jQuery('#buttonUp').css({
					'opacity':'0'
				});
				};
			});
	jQuery('#buttonUp').click(function() {
		$('body,html').animate({scrollTop: 0}, 400);
	});

	// Snowfall
	// $(document).snowfall({
 //        flakeCount: 150,
 //        flakeColor: '#c8dde9',
 //        minSize: 5,
 //        maxSize:10,
 //        round: true,
 //        shadow: false,
 //    });

 	/*======================================
 	=            Seqence Banner            =
 	======================================*/

 	if ($('div').is('#sequence')) {
 		// Get the Sequence element
		var sequenceElement = document.getElementById("sequence");

		// Place your Sequence options here to override defaults
		// See: http://sequencejs.com/documentation/#options
		var options = {
		  startingStepAnimatesIn: true,
		  //animateCanvas: false,
		  phaseThreshold: false,
		  //preloader: true,
		  //reverseWhenNavigatingBackwards: true,
		  autoPlay: true,
		  autoPlayInterval: 5000,
		  //nextButton: false,
		  //prevButton: false
		}

		// Launch Sequence on the element, and with the options we specified above
		var mySequence = sequence(sequenceElement, options);
 	}

 	/*=====  End of Seqence Banner  ======*/


	/*=============================
	=            Front            =
	=============================*/

	//Маска для ввода телефона
	var namePhone = $('[name=phone]'),
		nameEmail = $('[name=email]')
	;
	if ($('input').is('[name=phone]') && $('input').is('[name=email]')) {
		namePhone.inputmask("+7 (999) 999 99 99");
		nameEmail.inputmask({
	        mask: "*{1,64}@a{1,64}[.a{1,3}]",
	        definitions: {
	          '*': {
	            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
	            cardinality: 1,
	            casing: "lower"
	          }
	        }
	    });
	}

	// Проверка валидности Согласия клиента
	$( "input[type=checkbox]").click(function() {

		var submitForm = $( ".submit-form" );

		if ( submitForm.hasClass('submit-form--hidden' ) ) {
		  submitForm.removeAttr( "disabled" ).removeClass('submit-form--hidden');
		} else {
		  submitForm.addClass('submit-form--hidden').attr( {disabled: true} );
		}
	});

	//Шестеренки
 	// var cogwheel_1 = $(".repair-image_back_level1"),
	 // 	cogwheel_2 = $(".repair-image_back_level2");

 	// TweenMax.to(cogwheel_1, 20, {
 	// 	rotation: "360",
 	// 	repeat: -1,
 	// 	ease: Linear.easeNone
 	// });
 	// TweenMax.to(cogwheel_2, 10, {
 	// 	rotation: "360",
 	// 	repeat: -1,
 	// 	ease: Linear.easeNone
 	// });

	/*=====  End of Front  ======*/

	/*==============================
	=            Repair            =
	==============================*/

	//check if the .repair_photos_container is in the viewport
    //if yes, animate it
    // checkPosition(jQuery('.repair_photos_container'));
    // jQuery(window).on('scroll', function(){
    //     checkPosition(jQuery('.repair_photos_container'));
    // });

    // //make the .repair-handle element draggable and modify .repair-resize-img width according to its position
    // jQuery('.repair_photos_container').each(function(){
    //     var actual = jQuery(this);
    //     drags(actual.find('.repair-handle'), actual.find('.repair-resize-img'), actual, actual.find('.repair_photos_label[data-type="original"]'), actual.find('.repair_photos_label[data-type="modified"]'));
    // });

    // //upadate images label visibility
    // jQuery(window).on('resize', function(){
    //     jQuery('.repair_photos_container').each(function(){
    //         var actual = jQuery(this);
    //         updateLabel(actual.find('.repair_photos_label[data-type="modified"]'), actual.find('.repair-resize-img'), 'left');
    //         updateLabel(actual.find('.repair_photos_label[data-type="original"]'), actual.find('.repair-resize-img'), 'right');
    //     });
    // });

    // function checkPosition(container) {
    //     container.each(function(){
    //         var actualContainer = jQuery(this);
    //         if( jQuery(window).scrollTop() + jQuery(window).height()*0.5 > actualContainer.offset().top) {
    //             actualContainer.addClass('is-visible');
    //         }
    //     });
    // }

    // //draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
    // function drags(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
    //     dragElement.on("mousedown vmousedown", function(e) {
    //         dragElement.addClass('draggable');
    //         resizeElement.addClass('resizable');

    //         var dragWidth = dragElement.outerWidth(),
    //             xPosition = dragElement.offset().left + dragWidth - e.pageX,
    //             containerOffset = container.offset().left,
    //             containerWidth = container.outerWidth(),
    //             minLeft = containerOffset + 10,
    //             maxLeft = containerOffset + containerWidth - dragWidth - 10;

    //         dragElement.parents().on("mousemove vmousemove", function(e) {
    //             leftValue = e.pageX + xPosition - dragWidth;

    //             //constrain the draggable element to move inside his container
    //             if(leftValue < minLeft ) {
    //                 leftValue = minLeft;
    //             } else if ( leftValue > maxLeft) {
    //                 leftValue = maxLeft;
    //             }

    //             widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';

    //             jQuery('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
    //                 jQuery(this).removeClass('draggable');
    //                 resizeElement.removeClass('resizable');
    //             });

    //             jQuery('.resizable').css('width', widthValue);

    //             updateLabel(labelResizeElement, resizeElement, 'left');
    //             updateLabel(labelContainer, resizeElement, 'right');

    //         }).on("mouseup vmouseup", function(e){
    //             dragElement.removeClass('draggable');
    //             resizeElement.removeClass('resizable');
    //         });
    //         e.preventDefault();
    //     }).on("mouseup vmouseup", function(e) {
    //         dragElement.removeClass('draggable');
    //         resizeElement.removeClass('resizable');
    //     });
    // }

    // function updateLabel(label, resizeElement, position) {
    //     if(position == 'left') {
    //         ( label.offset().left + label.outerWidth() < resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
    //     } else {
    //         ( label.offset().left > resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
    //     }
    // }

	/*=====  End of Repair  ======*/

	//Переносы слов в русском
	var pHyph = $('p');
	if ($('div').is('p')) {
		pHyph.hyphenate();
	}


	/*----------  Lazy Load  ----------*/
	$('.lazy').Lazy({
		effect: "fadeIn",
		effectTime: 2000,
		visibleOnly: true
	});


}(jQuery));