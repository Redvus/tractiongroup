jQuery(document).ready(function($) {

	//Button Up
	jQuery('<div id="buttonUp"></div>').insertAfter('.wrapper');
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() >= 600) {
			jQuery('#buttonUp').css({
				'-webkit-transform': 'translateX(-20px)',
				'-moz-transform':'translateX(-20px)',
				'-o-transform':'translateX(-20px)',
				'transform':'translateX(-20px)',
				'opacity':'1'
			});
			}
			else {
				jQuery('#buttonUp').css({
					'-webkit-transform': 'translateX(20px)',
					'-moz-transform':'translateX(20px)',
					'-o-transform':'translateX(20px)',
					'transform':'translateX(20px)',
					'opacity':'0'
				});
				};
			});
	jQuery('#buttonUp').click(function() {
		$('body,html').animate({scrollTop: 0}, 400);
	});

	//Маска для ввода телефона
	$('[name=phone]').inputmask("+7 (999) 999 99 99");
	$('[name=email]').inputmask({
        mask: "*{1,64}@a{1,64}[.a{1,3}]",
        definitions: {
          '*': {
            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
            cardinality: 1,
            casing: "lower"
          }
        }
    });

	// Проверка валидности Согласия клиента
	$( "input[type=checkbox]").click(function() {

		var submitForm = $( ".submit-form" );

		if ( submitForm.hasClass('submit-form--hidden' ) ) {
		  submitForm.removeAttr( "disabled" ).removeClass('submit-form--hidden');
		} else {
		  submitForm.addClass('submit-form--hidden').attr( {disabled: true} );
		}
	});

	//Переносы слов в русском
	//$('p').hyphenate();

	//Snowfall
	// $(document).snowfall({
 //        flakeCount: 150,
 //        flakeColor: '#c8dde9',
 //        minSize: 5,
 //        maxSize:10,
 //        round: true,
 //        shadow: false,
 //    });

 	//Шестеренки
 	// var cogwheel_1 = $(".repair-image_back_level1"),
	 // 	cogwheel_2 = $(".repair-image_back_level2");

 	// TweenMax.to(cogwheel_1, 20, {
 	// 	rotation: "360",
 	// 	repeat: -1,
 	// 	ease: Linear.easeNone
 	// });
 	// TweenMax.to(cogwheel_2, 10, {
 	// 	rotation: "360",
 	// 	repeat: -1,
 	// 	ease: Linear.easeNone
 	// });

});