jQuery(document).ready(function($) {

	//Button Up
	jQuery('<div id="buttonUp"></div>').insertAfter('.wrapper');
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() >= 600) {
			jQuery('#buttonUp').css({
				'-webkit-transform': 'translateX(-20px)',
				'-moz-transform':'translateX(-20px)',
				'-o-transform':'translateX(-20px)',
				'transform':'translateX(-20px)',
				'opacity':'1'
			});
			}
			else {
				jQuery('#buttonUp').css({
					'-webkit-transform': 'translateX(20px)',
					'-moz-transform':'translateX(20px)',
					'-o-transform':'translateX(20px)',
					'transform':'translateX(20px)',
					'opacity':'0'
				});
				};
			});
	jQuery('#buttonUp').click(function() {
		$('body,html').animate({scrollTop: 0}, 400);
	});
	
	//Snowfall
	// $(document).snowfall({
 //        flakeCount: 150,
 //        flakeColor: '#c8dde9',
 //        minSize: 5, 
 //        maxSize:10,
 //        round: true,
 //        shadow: false,
 //    });
});